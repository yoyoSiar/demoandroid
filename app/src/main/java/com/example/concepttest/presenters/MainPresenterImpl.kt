package com.example.concepttest.presenters

import com.example.concepttest.interactors.MainInteractorImpl
import com.example.concepttest.interfaces.main.MainInteractor
import com.example.concepttest.interfaces.main.MainPresenter
import com.example.concepttest.interfaces.main.MainView
import com.example.concepttest.models.Post

class MainPresenterImpl: MainPresenter {

    private val TAG = "MainPresenterImpl"
    private var interactor: MainInteractor? = null
    private var view: MainView? = null



    constructor(view: MainView?) {
        this.view = view
        interactor = MainInteractorImpl(this)
    }

    override fun showList(lista: ArrayList<Post>) {
        view!!.showList(lista)
    }

    override fun showError(error: String?) {
        view!!.showError(error!!)
    }

    override fun loadPost() {
        interactor!!.cargarData()
    }
}