package com.example.concepttest.interactors

import com.example.concepttest.api.RetrofitClient
import com.example.concepttest.interfaces.main.MainInteractor
import com.example.concepttest.interfaces.main.MainPresenter
import com.example.concepttest.models.Post
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainInteractorImpl : MainInteractor {

    private val TAG = "MainInteractorImpl"

    private var listado: ArrayList<Post>? = null
    private var presenter: MainPresenter? = null


    constructor(presenter: MainPresenter){
        this.presenter = presenter
    }

    override fun cargarData() {
        listado = ArrayList()

        RetrofitClient.instance.getAllPost().enqueue(object : Callback<List<Post>> {
            override fun onFailure(call: Call<List<Post>>, t: Throwable) {
                presenter!!.showError(t.message)
            }

            override fun onResponse(call: Call<List<Post>>, response: Response<List<Post>>) {
                listado = response.body() as ArrayList<Post>?
                presenter!!.showList(listado!!)
            }
        })
    }

}