package com.example.concepttest.api

import com.example.concepttest.models.Post
import retrofit2.Call
import retrofit2.http.GET

interface PostApi {
    @GET("posts")
    fun getAllPost(): Call<List<Post>>
}