package com.example.concepttest.interfaces.main

import com.example.concepttest.models.Post

interface MainView {
    fun showList(lista: ArrayList<Post>)

    fun showError(error: String)
}