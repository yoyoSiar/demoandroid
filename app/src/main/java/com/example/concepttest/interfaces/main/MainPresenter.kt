package com.example.concepttest.interfaces.main

import com.example.concepttest.models.Post

interface MainPresenter {
    fun loadPost()

    fun showList(lista: ArrayList<Post>)

    fun showError(error: String?)
}