package com.example.concepttest.ui

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.concepttest.R
import com.example.concepttest.interfaces.main.MainPresenter
import com.example.concepttest.interfaces.main.MainView
import com.example.concepttest.models.Post
import com.example.concepttest.presenters.MainPresenterImpl
import com.example.concepttest.ui.uiAdapters.PostAdapter

class MainActivity : AppCompatActivity(), MainView {
    private val TAG = "MainActivity"

    private var presenter: MainPresenter? = null
    lateinit var rvPostsList : RecyclerView
    val pAdapter: PostAdapter = PostAdapter()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter = MainPresenterImpl(this)
        loadPost()
    }

    private fun loadPost() {
        presenter!!.loadPost()
    }

    override fun showList(lista: ArrayList<Post>) {
        rvPostsList = findViewById(R.id.rv_postlist) as RecyclerView
        rvPostsList.layoutManager = LinearLayoutManager(this)
        pAdapter.PostAdapter(lista, this)
        rvPostsList.adapter = pAdapter
    }

    override fun showError(error: String) {
        Log.e(TAG, error)
    }



    //Menu Toolbar
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        Toast.makeText(this, item.title, Toast.LENGTH_SHORT).show()
        return super.onOptionsItemSelected(item)
    }

}
