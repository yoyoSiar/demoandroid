package com.example.concepttest.ui.ui_fragments

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.concepttest.ui.fragments.DetalleFragment
import com.example.concepttest.ui.fragments.InfoFragment
import com.example.concepttest.ui.fragments.ListaFragment

class PageAdapter(fm: FragmentManager, private val myContext: Context, internal var totalTabs: Int) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        lateinit var fragment: Fragment
        when(position){
            0 -> fragment = ListaFragment()
            1 -> fragment = DetalleFragment()
            2 -> fragment = InfoFragment()
        }
        return fragment
    }

    override fun getCount(): Int {
        return totalTabs
    }
}