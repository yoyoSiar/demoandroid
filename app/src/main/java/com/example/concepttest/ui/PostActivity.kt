package com.example.concepttest.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import com.example.concepttest.R
import com.example.concepttest.ui.ui_fragments.PageAdapter
import com.google.android.material.tabs.TabItem
import com.google.android.material.tabs.TabLayout

class PostActivity : AppCompatActivity() {

    var tabLayout: TabLayout? = null
    private lateinit var viewPager: ViewPager
    private lateinit var pageAdapter: PageAdapter
    var tab_listado: TabItem? = null
    var tab_detalles: TabItem? = null
    var tab_info: TabItem? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post)

        tabLayout = findViewById(R.id.tablayout)
        viewPager = findViewById(R.id.viewpager)

        pageAdapter = PageAdapter(supportFragmentManager, this, tabLayout!!.tabCount)
        viewPager.adapter = pageAdapter

        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                viewPager.currentItem = tab!!.position
                if(tab.position == 0){
                    pageAdapter.notifyDataSetChanged()
                }
                if(tab.position == 1){
                    pageAdapter.notifyDataSetChanged()
                }
                if(tab.position == 2){
                    pageAdapter.notifyDataSetChanged()
                }
            }

        })

        viewPager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
    }
}
