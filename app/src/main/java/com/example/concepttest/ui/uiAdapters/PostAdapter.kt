package com.example.concepttest.ui.uiAdapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.concepttest.R
import com.example.concepttest.models.Post

class PostAdapter: RecyclerView.Adapter<PostAdapter.ViewHolder>() {
    var posts : ArrayList<Post> = ArrayList()
    lateinit var context: Context

    fun PostAdapter(listado: ArrayList<Post>, context: Context){
        this.posts = listado
        this.context = context
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = posts.get(position)
        holder.bind(item, context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.post_item, parent, false))
    }

    override fun getItemCount(): Int {
        return posts.size
    }



    class ViewHolder(v: View): RecyclerView.ViewHolder(v){
        val usuario = v.findViewById(R.id.user_id) as TextView
        val id = v.findViewById(R.id.post_id) as TextView
        val titulo = v.findViewById(R.id.post_title) as TextView

        fun bind(p: Post, ctx: Context){
            usuario.text = "Usuario: " + p.userId
            id.text = "Post: " + p.id
            titulo.text = "Titulo: " + p.title
            itemView.setOnClickListener {
                Toast.makeText(ctx, p.body, Toast.LENGTH_SHORT).show()
            }
        }
    }
}